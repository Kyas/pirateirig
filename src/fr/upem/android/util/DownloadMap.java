package fr.upem.android.util;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import fr.upem.android.game.R;
import fr.upem.android.game.model.Direction;
import fr.upem.android.game.model.EntityType;
import fr.upem.android.game.model.GameMap;
import fr.upem.android.game.model.Gravity;
import fr.upem.android.game.model.Pirate;
import fr.upem.android.game.model.Wall;

/**
 * DownloadMap - Asynctask Class where we generate the game Map (for the game).
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class DownloadMap extends AsyncTask<Void, Void, GameMap> {

	/**
	 * The context.
	 */
	private Context context;

	/**
	 * The content file.
	 */
	private String contentFile;

	/**
	 * The screen Width.
	 */
	private int screenWidth;

	/**
	 * The screen Height.
	 */
	private int screenHeight;

	/**
	 * The number of lines.
	 */
	private int numberOfLines;

	/**
	 * The number of Columns.
	 */
	private int numberOfColumns;

	/**
	 * The constant speed of a pirate.
	 */
	private final static int CONSTANT_SPEED = 7;

	/**
	 * The constant life of a pirate.
	 */
	private final static int INIT_LIFE = 3;

	/**
	 * The constructor.
	 * 
	 * @param context
	 *            The context.
	 * @param contentFile
	 *            The content file.
	 * @param screenWidth
	 *            The screen width.
	 * @param screenHeight
	 *            The screen height.
	 */
	public DownloadMap(Context context, String contentFile, int screenWidth,
			int screenHeight) {
		this.context = context;
		this.contentFile = contentFile;
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
	}

	@Override
	protected GameMap doInBackground(Void... params) {

		// It separates the String by \n.
		String[] tokens = contentFile.split("\\r?\\n");
		numberOfLines = tokens.length;
		numberOfColumns = tokens[0].length();

		/**
		 * Ratio of the resolution of the screen, we recovered the total size
		 * (Width & height) and divide by the size of the content (line /
		 * Column) to obtain the ratio.
		 */
		int ratioV = (screenHeight / numberOfLines) + 1;
		int ratioH = (screenWidth / numberOfColumns) + 1;

		// Initialization of array of two dimensions to fill the content file
		// into this array.
		char[][] arrayGame = new char[numberOfLines][numberOfColumns];

		// Filling the array of two dimensions horizontally.
		for (int i = 0; i < numberOfLines; i++) {
			String s = tokens[i];
			for (int j = 0; j < numberOfColumns; j++) {
				char c = s.charAt(j);
				arrayGame[i][j] = c;
			}
		}

		ArrayList<Wall> walls = new ArrayList<Wall>();
		Pirate[] pirates = new Pirate[2];

		// The ratio player. (dimension).
		int ratioPlayer = (screenWidth / screenHeight);

		for (int i = 0; i < numberOfLines; i++) {
			for (int j = 0; j < numberOfColumns; j++) {

				/**
				 * Walls
				 */
				if (arrayGame[i][j] == 'x') {
					walls.add(new Wall(0, ratioH, 0, ratioV, j * ratioH, i
							* ratioV, null, EntityType.WALL));
				}

				/**
				 * Players
				 */
				if (arrayGame[i][j] == '1') {
					Bitmap bitmapPlayerOne = BitmapFactory.decodeResource(
							context.getResources(), R.drawable.pirate_red);

					// The bitmap touched.
					Bitmap bitmapPlayerOneTouched = BitmapFactory
							.decodeResource(context.getResources(),
									R.drawable.pirate_red_touched);

					// The bitmap shield.
					Bitmap bitmapPlayerOneShield = BitmapFactory
							.decodeResource(context.getResources(),
									R.drawable.pirate_red_shield);

					bitmapPlayerOne = Bitmap.createScaledBitmap(
							bitmapPlayerOne, bitmapPlayerOne.getWidth()
									/ ratioPlayer, bitmapPlayerOne.getHeight()
									/ ratioPlayer, false);
					bitmapPlayerOneTouched = Bitmap.createScaledBitmap(
							bitmapPlayerOneTouched,
							bitmapPlayerOneTouched.getWidth() / ratioPlayer,
							bitmapPlayerOneTouched.getHeight() / ratioPlayer,
							false);
					bitmapPlayerOneShield = Bitmap.createScaledBitmap(
							bitmapPlayerOneShield,
							bitmapPlayerOneShield.getWidth() / ratioPlayer,
							bitmapPlayerOneShield.getHeight() / ratioPlayer,
							false);

					// Creation of pirate one.
					pirates[0] = createPirate(arrayGame, i, j, true, ratioH,
							ratioV, bitmapPlayerOne, bitmapPlayerOneTouched,
							bitmapPlayerOneShield);

				}

				if (arrayGame[i][j] == '2') {
					Bitmap bitmapPlayerTwo = BitmapFactory.decodeResource(
							context.getResources(), R.drawable.pirate_green);

					Bitmap bitmapPlayerTwoTouched = BitmapFactory
							.decodeResource(context.getResources(),
									R.drawable.pirate_green_touched);

					Bitmap bitmapPlayerTwoShield = BitmapFactory
							.decodeResource(context.getResources(),
									R.drawable.pirate_green_shield);

					bitmapPlayerTwo = Bitmap.createScaledBitmap(
							bitmapPlayerTwo, bitmapPlayerTwo.getWidth()
									/ ratioPlayer, bitmapPlayerTwo.getHeight()
									/ ratioPlayer, false);

					bitmapPlayerTwoTouched = Bitmap.createScaledBitmap(
							bitmapPlayerTwoTouched,
							bitmapPlayerTwoTouched.getWidth() / ratioPlayer,
							bitmapPlayerTwoTouched.getHeight() / ratioPlayer,
							false);

					bitmapPlayerTwoShield = Bitmap.createScaledBitmap(
							bitmapPlayerTwoShield,
							bitmapPlayerTwoShield.getWidth() / ratioPlayer,
							bitmapPlayerTwoShield.getHeight() / ratioPlayer,
							false);

					// Creation of pirate two.
					pirates[1] = createPirate(arrayGame, i, j, false, ratioH,
							ratioV, bitmapPlayerTwo, bitmapPlayerTwoTouched,
							bitmapPlayerTwoShield);

				}

			}
		}

		// Creation of a single instance of GameMap.
		GameMap map = GameMap.getInstance();
		map.addWalls(walls);
		map.addPirates(pirates);
		map.addRatioH(ratioH);
		map.addRatioV(ratioV);
		map.addArrayGame(arrayGame);

		return map;
	}

	/**
	 * Determine the case (which wall does the pirate is located on) for the
	 * creation of a Pirate (gravity, direction).
	 * 
	 * @param arrayGame
	 *            The array of two dimensions.
	 * @param i
	 *            The index of a line.
	 * @param j
	 *            The index of a column.
	 * @return the case.
	 */
	private int pirateByWhichWall(char[][] arrayGame, int i, int j) {
		boolean isWallLeft = false;
		boolean isWallRight = false;
		boolean isWallTop = false;
		boolean isWallBottom = false;

		if (arrayGame[i][j - 1] == 'x') {
			isWallLeft = true;
		}
		if (arrayGame[i][j + 1] == 'x') {
			isWallRight = true;
		}
		if (arrayGame[i - 1][j] == 'x') {
			isWallTop = true;
		}
		if (arrayGame[i + 1][j] == 'x') {
			isWallBottom = true;
		}

		if (isWallLeft) {
			if (!isWallRight && !isWallTop && !isWallBottom) {
				return 0;
			}
			if (isWallRight && !isWallTop && !isWallBottom) {
				return 1;
			}
			if (!isWallRight && isWallTop && !isWallBottom) {
				return 2;
			}
			if (!isWallRight && !isWallTop && isWallBottom) {
				return 3;
			}
		} else if (isWallRight) {
			if (!isWallLeft && !isWallTop && !isWallBottom) {
				return 4;
			}
			if (!isWallLeft && isWallTop && !isWallBottom) {
				return 5;
			}
			if (!isWallLeft && !isWallTop && isWallBottom) {
				return 6;
			}
		} else if (isWallTop) {
			if (!isWallLeft && !isWallRight && !isWallBottom) {
				return 7;
			}
			if (!isWallLeft && !isWallRight && isWallBottom) {
				return 8;
			}
		} else if (isWallBottom) {
			if (!isWallLeft && !isWallRight && !isWallTop) {
				return 9;
			}
		} else if (isWallLeft && isWallTop && isWallBottom) {
			return 10;
		} else if (isWallLeft && isWallTop && isWallRight) {
			return 11;
		} else if (isWallTop && isWallRight && isWallBottom) {
			return 12;
		} else if (isWallLeft && isWallBottom && isWallRight) {
			return 13;
		}

		return -1;
	}

	/**
	 * Create the pirate.
	 * 
	 * @param arrayGame
	 *            The array of two dimensions.
	 * @param i
	 *            The index of line.
	 * @param j
	 *            The index of column.
	 * @param isPlayerOne
	 *            If it's the player one or two.
	 * @param ratioH
	 *            The ratio (horizontal).
	 * @param ratioV
	 *            The ratio (vertical).
	 * @param bitmap
	 *            The bitmap.
	 * @param bmpTouched
	 *            The bitmap touched.
	 * @param bmpShield
	 *            The bitmap shield.
	 * @return the pirate.
	 */
	private Pirate createPirate(char[][] arrayGame, int i, int j,
			boolean isPlayerOne, int ratioH, int ratioV, Bitmap bitmap,
			Bitmap bmpTouched, Bitmap bmpShield) {

		EntityType type = null;
		if (isPlayerOne) {
			type = EntityType.PLAYER_ONE;
		} else {
			type = EntityType.PLAYER_TWO;
		}
		Gravity gravity = null;
		Direction direction = null;

		// retrieve the case.
		int mode = pirateByWhichWall(arrayGame, i, j);
		switch (mode) {
		case 0:
		case 1:
		case 2:
		case 11:
			gravity = Gravity.VERTICAL_LEFT;
			direction = Direction.DOWN;
			break;
		case 3:
			gravity = Gravity.HORIZONTAL_UP;
			direction = Direction.RIGHT;
			break;
		case 4:
		case 5:
			gravity = Gravity.VERTICAL_RIGHT;
			direction = Direction.DOWN;
			break;
		case 6:
		case 9:
		case 12:
			gravity = Gravity.HORIZONTAL_DOWN;
			direction = Direction.RIGHT;
			break;
		case 7:
		case 8:
		case 10:
			gravity = Gravity.HORIZONTAL_UP;
			direction = Direction.RIGHT;
			break;
		case 13:
			gravity = Gravity.VERTICAL_LEFT;
			direction = Direction.UP;
			break;
		}

		return new Pirate(j * ratioH, i * ratioV, bitmap, gravity, direction,
				type, CONSTANT_SPEED, INIT_LIFE, bmpTouched, bmpShield);
	}
}
