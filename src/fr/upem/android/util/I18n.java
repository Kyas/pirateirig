package fr.upem.android.util;

import java.util.Locale;

import android.content.Context;
import android.content.res.Configuration;

/**
 * Internationalization (for the langage).
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class I18n {

	/**
	 * The constant string for French.
	 */
	public final static String LANG_FR = "fr";

	/**
	 * The constant string for English.
	 */
	public final static String LANG_EN = "en";

	/**
	 * The langage.
	 */
	public static String langage = null;

	/**
	 * Update the langage.
	 * 
	 * @param context
	 *            The context.
	 * @param langage
	 *            The langage.
	 */
	public static void updateLangage(Context context, String langage) {
		Locale locale = new Locale(langage);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		context.getApplicationContext().getResources()
				.updateConfiguration(config, null);
		I18n.langage = langage;
	}

	/**
	 * Get the langage.
	 * 
	 * @return The langage.
	 */
	public static String getLangage() {
		if (I18n.langage == null) {
			I18n.langage = LANG_FR;
			return I18n.langage;
		}
		return I18n.langage;
	}

}
