package fr.upem.android.util;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Displays - Utility class for the screen dimension.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class Displays {

	/**
	 * For the density (size of the screen for Gameview) to To get the right
	 * resolution objects (wall) in the playground
	 * 
	 * @param context
	 * @return
	 */
	@SuppressWarnings("unused")
	private static String getDensityName(Context context) {
		float density = context.getResources().getDisplayMetrics().density;
		if (density >= 4.0) {
			return "xxxhdpi";
		}
		if (density >= 3.0) {
			return "xxhdpi";
		}
		if (density >= 2.0) {
			return "xhdpi";
		}
		if (density >= 1.5) {
			return "hdpi";
		}
		if (density >= 1.0) {
			return "mdpi";
		}
		return "ldpi";
	}

	/**
	 * Return the size of the notification bar.
	 * 
	 * @param context
	 *            The context.
	 * @return The size.
	 */
	public static int getStatusBarHeight(Context context) {
		int result = 0;
		int resourceId = context.getResources().getIdentifier(
				"status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = context.getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

	/**
	 * Return the screen dimensions (width/height/density).
	 * 
	 * @param context
	 *            The context.
	 * @return The size dimensions.
	 */
	public static int[] getScreenSize(Context context) {
		int[] screenSizes = new int[3];

		DisplayMetrics metrics = new DisplayMetrics();
		Activity activity = (Activity) context;
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

		// Tablette (42 - Hauteur de la navigation bar et 13 - Hauteur de la
		// barre joueurs).
		// Bug spotted ici because CustomView did not take good dimension
		// (height).
		screenSizes[0] = metrics.widthPixels;
		screenSizes[1] = metrics.heightPixels - getStatusBarHeight(context)
				- 42 - 26;
		screenSizes[2] = metrics.densityDpi;

		return screenSizes;
	}

}
