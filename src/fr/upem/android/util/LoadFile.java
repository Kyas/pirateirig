package fr.upem.android.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import fr.upem.android.game.R;
import fr.upem.android.game.model.Level;

/**
 * LoadFile : Load the file (content).
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class LoadFile {

	/**
	 * Return the list of Level in /res/raw folder.
	 * 
	 * @return the list of Level.
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static List<Level> listRaw() throws IllegalArgumentException,
			IllegalAccessException {
		final ArrayList<Level> arrayList = new ArrayList<Level>();
		Field[] fields = R.raw.class.getFields();
		for (int count = 0; count < fields.length; count++) {
			Level l = new Level(fields[count].getInt(fields[count]),
					fields[count].getName());
			arrayList.add(l);
		}
		return arrayList;
	}

	/**
	 * Return the string of a content file.
	 * 
	 * @param inputstream
	 *            The inputStream.
	 * @param isOverview
	 *            If it's an overview of the contentFile.
	 * @return the string (content file).
	 * @throws IOException
	 */
	public static String readTextFile(InputStream inputstream,
			boolean isOverview) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputstream));
		StringBuilder fileContents = new StringBuilder();

		int c;
		while ((c = reader.read()) != -1) {
			char character = (char) c;
			if (isOverview) {
				if (Character.isWhitespace(character)) {
					fileContents.append(' ');
				}
			}
			fileContents.append(character);
		}
		return fileContents.toString();
	}
}
