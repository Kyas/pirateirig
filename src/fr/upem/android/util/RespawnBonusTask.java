package fr.upem.android.util;

import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import fr.upem.android.game.R;
import fr.upem.android.game.model.Bonus;
import fr.upem.android.game.model.EntityType;
import fr.upem.android.game.model.bonus.Bomb;
import fr.upem.android.game.model.bonus.Life;
import fr.upem.android.game.model.bonus.Shield;
import fr.upem.android.game.model.bonus.Turbo;

/**
 * RespawnBonusTask : To respawn a Bonus on the map (random position / random
 * bonus type).
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class RespawnBonusTask extends AsyncTask<Object, Void, Bonus> {

	@Override
	protected Bonus doInBackground(Object... params) {
		final Context context = (Context) params[0];
		final int ratioH = (Integer) params[1];
		final int ratioV = (Integer) params[2];
		final int ratioEntity = (Integer) params[3];
		final char[][] arrayGame = (char[][]) params[4];
		final int numberOfLines = arrayGame.length;
		final int numberOfColumns = arrayGame[0].length;
		int bonusId;
		Bitmap bitmapBonus = null;

		Random random = new Random();
		bonusId = random.nextInt(4);

		/**
		 * Faire le random.
		 */
		while (true) {
			// Number of lines.
			int maxLines = numberOfLines;

			// Number of columns
			int maxColumns = numberOfColumns;

			int i = random.nextInt(maxLines);
			int j = random.nextInt(maxColumns);

			// Random of a position. If this position is not on a wall, we
			// create a random Bonus.
			if (arrayGame[i][j] != 'x') {
				Bonus bonus = null;
				switch (bonusId) {
				case 0:
					bitmapBonus = BitmapFactory.decodeResource(
							context.getResources(), R.drawable.shield);
					bitmapBonus = Bitmap.createScaledBitmap(bitmapBonus,
							bitmapBonus.getWidth() / ratioEntity,
							bitmapBonus.getHeight() / ratioEntity, false);
					bonus = new Shield(j * ratioH, i * ratioV, bitmapBonus,
							EntityType.SHIELD, System.currentTimeMillis());
					break;
				case 1:
					bitmapBonus = BitmapFactory.decodeResource(
							context.getResources(), R.drawable.life);
					bitmapBonus = Bitmap.createScaledBitmap(bitmapBonus,
							bitmapBonus.getWidth() / ratioEntity,
							bitmapBonus.getHeight() / ratioEntity, false);
					bonus = new Life(j * ratioH, i * ratioV, bitmapBonus,
							EntityType.LIFE, System.currentTimeMillis());
					break;
				case 2:
					bitmapBonus = BitmapFactory.decodeResource(
							context.getResources(), R.drawable.bomb);
					bitmapBonus = Bitmap.createScaledBitmap(bitmapBonus,
							bitmapBonus.getWidth() / ratioEntity,
							bitmapBonus.getHeight() / ratioEntity, false);
					bonus = new Bomb(j * ratioH, i * ratioV, bitmapBonus,
							EntityType.BOMB, System.currentTimeMillis());
					break;
				case 3:
					bitmapBonus = BitmapFactory.decodeResource(
							context.getResources(), R.drawable.turbo);
					bitmapBonus = Bitmap.createScaledBitmap(bitmapBonus,
							bitmapBonus.getWidth() / ratioEntity,
							bitmapBonus.getHeight() / ratioEntity, false);
					bonus = new Turbo(j * ratioH, i * ratioV, bitmapBonus,
							EntityType.TURBO, System.currentTimeMillis());
					break;
				}
				return bonus;
			}
		}

	}

}
