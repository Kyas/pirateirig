package fr.upem.android.game.graphics;

import android.graphics.Canvas;

/**
 * The sprite (to draw entities).
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public interface Sprite {

	/**
	 * Draw the entity.
	 * 
	 * @param canvas
	 *            The canvas.
	 */
	public void onDrawEntity(Canvas canvas);

}