package fr.upem.android.game;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
//TODO Useless
//import fr.upem.android.util.Displays;
import fr.upem.android.util.I18n;

/**
 * GameActivity. Displays the game activity.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class GameActivity extends Activity {

	/**
	 * The gameView.
	 */
	private GameView gameView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// If we are in a GAME Mode or PRACTICE Mode.
		final boolean isPracticeMode = getIntent().getExtras().getBoolean(
				"isPracticeMode");

		// Internationalization i18n.
		String langage = I18n.getLangage();
		I18n.updateLangage(getBaseContext(), langage);

		// Full screen.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_game);

		// Display the view Practice Mode.
		if (isPracticeMode == true) {
			View informationsBar = (View) findViewById(R.id.game_informations_bar);
			informationsBar.setVisibility(View.GONE);
			View informationsBarPractice = (View) findViewById(R.id.game_informations_bar_practice);
			informationsBarPractice.setVisibility(View.VISIBLE);
		}

		gameView = (GameView) findViewById(R.id.game_gameview);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	/**
	 * Display the life of the pirates.
	 * 
	 * @param playerOneLife
	 *            The player one's life.
	 * @param playerTwoLife
	 *            The player two's life.
	 */
	public void setPlayerLifeTextView(final int playerOneLife,
			final int playerTwoLife) {
		final TextView playerOneTv = (TextView) findViewById(R.id.game_pirate_one);
		final TextView playerTwoTv = (TextView) findViewById(R.id.game_pirate_two);

		// Run on the UI Thread to change TextView.
		GameActivity.this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				playerOneTv.setText(String.format(
						getResources().getString(R.string.game_pirate_one),
						playerOneLife));

				playerTwoTv.setText(String.format(
						getResources().getString(R.string.game_pirate_two),
						playerTwoLife));
			}
		});
	}

	/**
	 * Display the time.
	 * 
	 * @param minutes
	 *            The minutes.
	 * @param seconds
	 *            The seconds.
	 */
	public void setTimerTextView(final int minutes, final int seconds) {
		final TextView timerTv = (TextView) findViewById(R.id.game_timer);

		// Run on the UI Thread to change TextView.
		GameActivity.this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				timerTv.setText(String.format(
						getResources().getString(R.string.game_timer)
								+ " %02d:%02d", minutes, seconds));
			}
		});
	}

	/**
	 * Menu.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.game, menu);
		return true;
	}

	/**
	 * Item Selection Menu.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_pause:
			// If we pause the game.

			gameView.onPause();
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.menu_pause);
			builder.setPositiveButton(R.string.game_alert_pause_back,
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							gameView.onResume();
						}
					});
			builder.setIcon(R.drawable.ic_launcher);
			builder.create().show();
			return true;
		case R.id.menu_restart:
			// If we restart the game.

			AlertDialog.Builder builderRestart = new AlertDialog.Builder(this);
			builderRestart.setTitle(R.string.menu_restart);
			builderRestart.setNegativeButton(R.string.game_alert_cancel, null);
			builderRestart.setPositiveButton(R.string.menu_restart,
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = getIntent();
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK
									| Intent.FLAG_ACTIVITY_NO_ANIMATION);
							finish();
							overridePendingTransition(android.R.anim.fade_in, 0);

							// Stop the timerTask.
							gameView.stopTimerTask();
							startActivity(intent);
						}
					});

			builderRestart.setMessage(R.string.game_alert_restart);
			builderRestart.setIcon(R.drawable.ic_launcher);
			builderRestart.create().show();
			return true;
		case R.id.menu_langage:
			// If we change the langage.

			AlertDialog.Builder builderLangage = new AlertDialog.Builder(this);
			builderLangage.setTitle(R.string.menu_langage);
			builderLangage.setNegativeButton(R.string.game_alert_cancel, null);
			builderLangage.setPositiveButton(R.string.game_alert_ok,
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Context context = getApplicationContext();
							String langage = I18n.getLangage();
							if (langage.equals("fr")) {
								I18n.updateLangage(context, "en");
							} else {
								I18n.updateLangage(context, "fr");
							}
							Intent intent = getIntent();
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK
									| Intent.FLAG_ACTIVITY_NO_ANIMATION);
							finish();
							overridePendingTransition(android.R.anim.fade_in, 0);
							// Stop the timerTask.
							gameView.stopTimerTask();
							startActivity(intent);
						}
					});

			builderLangage.setMessage(R.string.game_alert_langage);
			builderLangage.setIcon(R.drawable.ic_launcher);
			builderLangage.create().show();
			return true;
		case R.id.menu_main:
			// If we go back to the main menu.

			AlertDialog.Builder builderMainMenu = new AlertDialog.Builder(this);
			builderMainMenu.setTitle(R.string.menu_main);
			builderMainMenu.setNegativeButton(R.string.game_alert_cancel, null);
			builderMainMenu.setPositiveButton(R.string.game_alert_ok,
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(GameActivity.this,
									MainActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK
									| Intent.FLAG_ACTIVITY_NO_ANIMATION);
							overridePendingTransition(android.R.anim.fade_in, 0);
							// Stop the timerTask.
							gameView.stopTimerTask();
							startActivity(intent);
						}
					});

			builderMainMenu.setMessage(R.string.game_alert_main_menu);
			builderMainMenu.setIcon(R.drawable.ic_launcher);
			builderMainMenu.create().show();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * If we click the back button, we go back to menu.
	 */
	@Override
	public void onBackPressed() {
		AlertDialog.Builder builderMainMenu = new AlertDialog.Builder(this);
		builderMainMenu.setTitle(R.string.menu_main);
		builderMainMenu.setNegativeButton(R.string.game_alert_cancel, null);
		builderMainMenu.setPositiveButton(R.string.game_alert_ok,
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						overridePendingTransition(android.R.anim.fade_in, 0);
						// Stop the timerTask.
						gameView.stopTimerTask();
						startActivity(new Intent(GameActivity.this,
								MainActivity.class)
								.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
										| Intent.FLAG_ACTIVITY_NEW_TASK));
					}
				});

		builderMainMenu.setMessage(R.string.game_alert_main_menu);
		builderMainMenu.setIcon(R.drawable.ic_launcher);
		builderMainMenu.create().show();

		return;
	}
}
