package fr.upem.android.game.model;

/**
 * The level model (contains a resource id and a filename).
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class Level {

	/**
	 * The resource Id.
	 */
	private int resId;

	/**
	 * The filename.
	 */
	private String filename;

	/**
	 * The constructor.
	 * 
	 * @param resId
	 *            The resourceId.
	 * @param filename
	 *            The filename.
	 */
	public Level(int resId, String filename) {
		this.resId = resId;
		this.filename = filename;
	}

	@Override
	public String toString() {
		return filename;
	}

	public String getFilename() {
		return filename;
	}

	public int getResId() {
		return resId;
	}
}
