package fr.upem.android.game.model;

/**
 * Gravity model : different gravities of a player
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public enum Gravity {

	VERTICAL_LEFT("vertical_left"), VERTICAL_RIGHT("vertical_right"), HORIZONTAL_UP(
			"horizontal_up"), HORIZONTAL_DOWN("horizontal_down");

	private String name = "";

	Gravity(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
