package fr.upem.android.game.model;

/**
 * Direction model : Different possible directions for a pirate.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public enum Direction {
	LEFT(-1), RIGHT(1), UP(-1), DOWN(1);

	private int direction = 0;

	/**
	 * Assigns a direction
	 * 
	 * @param direction
	 *            new direction
	 */
	Direction(int direction) {
		this.direction = direction;
	}

	public int getDirection() {
		return direction;
	}
}
