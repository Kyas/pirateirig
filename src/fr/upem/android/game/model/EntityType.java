package fr.upem.android.game.model;

/**
 * The entity type model - Specifies the type of an entity.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public enum EntityType {

	PLAYER_ONE("Player1"), PLAYER_TWO("Player2"), WALL("Wall"), BOMB("Bomb"), LIFE(
			"Life"), SHIELD("Shield"), TURBO("Turbo");

	private String name = "";

	EntityType(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
