package fr.upem.android.game.model;

import java.util.ArrayList;

/**
 * The GameMap model (Singleton class).
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class GameMap {
	/**
	 * List of walls.
	 */
	private ArrayList<Wall> walls;

	/**
	 * List of Pirates.
	 */
	private Pirate[] pirates;

	/**
	 * Ratio (horizontal).
	 */
	private int ratioH;

	/**
	 * Ratio (vertical).
	 */
	private int ratioV;

	/**
	 * Array Game.
	 */
	private char[][] arrayGame;

	/**
	 * Private constructor.
	 */
	private GameMap() {
	}

	/**
	 * MapHolder
	 * 
	 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
	 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
	 * @version 1.0
	 * 
	 */
	private static class MapHolder {
		/**
		 * Single instance not pre-initialized
		 * */
		private final static GameMap instance = new GameMap();
	}

	/** Point of access to the single instance of the singleton */
	public static GameMap getInstance() {
		return MapHolder.instance;
	}

	@Override
	public String toString() {
		return "Walls : " + walls.size() + " Players : " + pirates.length;
	}

	public void addWalls(ArrayList<Wall> walls) {
		this.walls = walls;
	}

	public void addPirates(Pirate[] pirates) {
		this.pirates = pirates;
	}

	public void addRatioH(int ratioH) {
		this.ratioH = ratioH;
	}

	public void addRatioV(int ratioV) {
		this.ratioV = ratioV;
	}

	public void addArrayGame(char[][] arrayGame) {
		this.arrayGame = arrayGame;
	}

	public ArrayList<Wall> getWalls() {
		return walls;
	}

	public Pirate[] getPirates() {
		return pirates;
	}

	public int getRatioH() {
		return ratioH;
	}

	public int getRatioV() {
		return ratioV;
	}

	public char[][] getArrayGame() {
		return arrayGame;
	}
}
