package fr.upem.android.game.model.bonus;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import fr.upem.android.collision.BoundingBox;
import fr.upem.android.game.model.Bonus;
import fr.upem.android.game.model.EntityType;

/**
 * Shield model (Bonus) - Protect a pirate.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class Shield extends Bonus {

	/**
	 * The constructor.
	 * 
	 * @param x
	 *            Coordinate x.
	 * @param y
	 *            Coordinate y.
	 * @param bitmap
	 *            The bitmap.
	 * @param entityType
	 *            The entity Type.
	 * @param timer
	 *            The timer.
	 */
	public Shield(int x, int y, Bitmap bitmap, EntityType entityType, long timer) {
		super(x, y, bitmap, entityType, timer);
		this.x = x;
		this.y = y;
		this.bitmap = bitmap;
		this.entityType = entityType;
		this.box = new BoundingBox(x, y, bitmap.getWidth(), bitmap.getHeight());
	}

	@Override
	public void onDrawEntity(Canvas canvas) {
		Paint p = new Paint();
		p.setAntiAlias(true);
		long current = System.currentTimeMillis();
		timeElapsed = (int) (current - timer) / 1000;
		if (timeElapsed >= 10 && timeElapsed <= 15) {
			isBlink = true;
			if (timeElapsed % 2 == 0) {
				canvas.drawBitmap(bitmap, x, y, p);
			}
		} else if (timeElapsed > 15) {
			isActive = false;
		} else {
			canvas.drawBitmap(bitmap, x, y, p);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Bonus : ").append("(").append(super.toString()).append(")");
		return sb.toString();
	}
}