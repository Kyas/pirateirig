package fr.upem.android.game.model;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import fr.upem.android.game.R;
import fr.upem.android.util.LoadFile;

/**
 * The level Adapter - Displays the Selection List.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class LevelAdapter extends ArrayAdapter<Level> {

	/**
	 * The context.
	 */
	private final Context context;

	/**
	 * The list of levels (retrieve from the /raw folder).
	 */
	private List<Level> levels = LoadFile.listRaw();

	/**
	 * The constructor.
	 * 
	 * @param context
	 *            The context.
	 * @param levels
	 *            The list of levels.
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public LevelAdapter(Context context, List<Level> levels)
			throws IllegalArgumentException, IllegalAccessException {
		super(context, R.layout.activity_levels_item, levels);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView;
		if (convertView == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater li = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = li.inflate(R.layout.activity_levels_item, null);
		} else {
			rowView = (View) convertView;
		}

		// Retrieve the level.
		Level level = getItem(position);

		// Retrieve the inputstream RAW file.txt in res/raw/ folder.
		InputStream is = context.getResources().openRawResource(
				levels.get(position).getResId());
		String overview = null;
		try {
			overview = LoadFile.readTextFile(is, true);
		} catch (IOException e) {
			Log.e("IOException", e.getLocalizedMessage());
		}

		TextView textViewOverview = (TextView) rowView
				.findViewById(R.id.selection_level_item_overview);
		textViewOverview.setText(overview);
		textViewOverview.setTypeface(null, Typeface.BOLD);

		TextView textView = (TextView) rowView
				.findViewById(R.id.selection_level_item);
		textView.setText(level.getFilename());
		textView.setTypeface(null, Typeface.BOLD);

		return rowView;
	}
}
