package fr.upem.android.game.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import fr.upem.android.collision.BoundingBox;
import fr.upem.android.collision.Movable;

/**
 * Pirate model.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class Pirate extends Entity implements Movable {

	private int speed;
	private Gravity gravity;
	private Direction direction;
	private int life;
	private boolean onWall;
	private int angle;

	private Matrix matrix;
	private Bitmap bmpTouched;
	private Bitmap bmpShield;
	private boolean isShield;
	private boolean isTouched;

	private boolean isJump = false;
	private boolean isfall = false;
	private double coefJump = 0;
	private double widthJump = 0;
	private double absPosition = 0;
	private long startFast = 0;
	private long startShield = 0;

	private final static int CONSTENT_SPEED = 7;
	
	public Pirate(int x, int y, Bitmap bitmap, Gravity gravity,
			Direction direction, EntityType entityType, int speed, int life,
			Bitmap bmpTouched, Bitmap bmpShield) {

		super(x, y, bitmap, entityType);
		this.x = x;
		this.y = y;
		this.bitmap = bitmap;
		this.gravity = gravity;
		this.direction = direction;
		this.entityType = entityType;
		this.box = new BoundingBox(x, y, bitmap.getWidth(), bitmap.getHeight());
		this.speed = speed;
		this.life = life;
		this.matrix = new Matrix();
		this.isShield = false;
		this.isTouched = false;
		this.bmpTouched = bmpTouched;
		this.bmpShield = bmpShield;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Gravity getGravity() {
		return gravity;
	}

	public void switchGravity() {
		switch (gravity) {
		case HORIZONTAL_DOWN:
			gravity = Gravity.HORIZONTAL_UP;
			break;
		case HORIZONTAL_UP:
			gravity = Gravity.HORIZONTAL_DOWN;
			break;
		case VERTICAL_LEFT:
			gravity = Gravity.VERTICAL_RIGHT;
			break;
		case VERTICAL_RIGHT:
			gravity = Gravity.VERTICAL_LEFT;
			break;
		default:
			break;
		}
	}
	
	public Direction getDirection() {
		return direction;
	}

	public void switchDirection() {
		switch (direction) {
		case LEFT:
			direction = Direction.RIGHT;
			break;
		case RIGHT:
			direction = Direction.LEFT;
			break;
		case UP:
			direction = Direction.DOWN;
			break;
		case DOWN:
			direction = Direction.UP;
			break;
		default:
			break;
		}
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getLife() {
		return life;
	}

	public void incrementeLife() {
		this.life = life + 1;
	}

	public void decrementeLife() {
		this.life = life - 1;
	}

	public boolean onWall() {
		return this.onWall;
	}

	public void setOnWall(boolean onTheWall) {
		this.onWall = onTheWall;
	}

	public boolean isFall() {
		return isfall;
	}

	public void setFall(boolean fall) {
		this.isfall = fall;
	}

	public boolean isJump() {
		return isJump;
	}

	@Override
	public void onDrawEntity(Canvas canvas) {
		Paint paint = new Paint();
		// matrix.reset();
		matrix.setTranslate(x, y);
		matrix.postRotate(angle, x + bitmap.getWidth() / 2,
				y + bitmap.getHeight() / 2);

		if (isTouched) {
			canvas.drawBitmap(bmpTouched, matrix, paint);
			isTouched = false;
		} else if (isShield) {
			canvas.drawBitmap(bmpShield, matrix, paint);
		} else {
			canvas.drawBitmap(bitmap, matrix, new Paint());
		}
	}

	@Override
	public void move() {

		// Gestion du goFast
		if (startFast != 0) {
			long current = System.currentTimeMillis();

			if (((current - startFast) / 1000) > 10) {
				speed = CONSTENT_SPEED;
				startFast = 0;
			} else {
				speed = 2 * CONSTENT_SPEED;
			}
		}

		// Gestion du shield
		if (startShield != 0) {
			long current = System.currentTimeMillis();

			if (((current - startShield) / 1000) > 10) {
				isShield = false;
				startShield = 0;
			} else {
				isShield = true;
			}
		}

		// Deplacement normal
		int step = direction.getDirection() * speed;
		int xNew = x;
		int yNew = y;

		angle += step;
		// Deplacement horizontal
		if ((gravity == Gravity.HORIZONTAL_DOWN)
				|| (gravity == Gravity.HORIZONTAL_UP)) {
			xNew = x + step;
		}
		// Deplacement vertical
		else if ((gravity == Gravity.VERTICAL_LEFT)
				|| (gravity == Gravity.VERTICAL_RIGHT)) {
			yNew = y + step;
		} else {
			return;
		}

		moveAt(xNew - x, yNew - y);
	}

	@Override
	public void moveAt(int directionX, int directionY) {

		x += directionX;
		box.setX(x);

		y += directionY;
		box.setY(y);

	}

	public void fall() {
		isfall = true;
		speed += 1;

		switch (gravity) {
		case HORIZONTAL_DOWN:
			moveAt(0, +speed);
			break;

		case HORIZONTAL_UP:
			moveAt(0, -speed);
			break;

		case VERTICAL_LEFT:
			moveAt(-speed, 0);
			break;

		case VERTICAL_RIGHT:
			moveAt(+speed, 0);
			break;

		default:
			break;
		}
	}

	public boolean jump(boolean jump) {

		isJump = jump;

		if (coefJump == 0) {
			widthJump = box.getWidth() * 2;
			absPosition = widthJump;
			coefJump = ((-widthJump) / (absPosition * absPosition));
		}

		int dest = calcJump(absPosition, coefJump, widthJump);
		int oldDest = dest;

		if (absPosition != widthJump) {
			oldDest = calcJump(absPosition + speed, coefJump, widthJump);
		}

		if ((absPosition <= -widthJump) || (!jump)) {
			coefJump = 0;
			widthJump = 0;
			absPosition = 0;

			return false;
		}

		if (absPosition > 0) {
			speed += 1;
		} else {
			speed -= 1;
		}

		switch (gravity) {
		case HORIZONTAL_DOWN:
			moveAt((direction.getDirection() * speed), (oldDest - dest));
			break;

		case HORIZONTAL_UP:
			moveAt((direction.getDirection() * speed), (dest - oldDest));
			break;

		case VERTICAL_LEFT:
			moveAt((dest - oldDest), (direction.getDirection() * speed));
			break;

		case VERTICAL_RIGHT:
			moveAt((oldDest - dest), (direction.getDirection() * speed));
			break;

		default:
			break;
		}

		absPosition = absPosition - speed;

		return true;
	}

	private int calcJump(double x, double a, double c) {
		return (int) ((x * x * a) + c);
	}

	public void goFast(long start, boolean force) {
		if (force) {
			this.startFast = start;
		} else if (absPosition < 0) {
			this.startFast = start;
		}
	}

	public void setShield(boolean isShield, long start) {
		this.isShield = isShield;
		this.startShield = start;
	}

	public boolean switchGravityAndDirection(BoundingBox bbox) {

		// maxgauche=max(x1,x2)
		// mindroit=min(x1+largeur1,x2+largeur2)
		// maxbas=max(y1,y2)
		// minhaut=min(y1+hauteur1,y2+hauteur2)

		int left = Math.max(box.getX(), bbox.getX());
		int right = Math.min(box.getX() + box.getWidth(),
				bbox.getX() + bbox.getWidth());
		int botom = Math.min(box.getY() + box.getHeight(),
				bbox.getY() + bbox.getHeight());
		int top = Math.max(box.getY(), bbox.getY());

		int width = right - left;
		int height = botom - top;

		if (width < height) {

			if (gravity == Gravity.HORIZONTAL_DOWN) {
				if (direction == Direction.LEFT) {
					gravity = Gravity.VERTICAL_LEFT;
				} else if (direction == Direction.RIGHT) {
					gravity = Gravity.VERTICAL_RIGHT;
				}

				if (absPosition > 0) {
					direction = Direction.UP;
				} else {
					direction = Direction.DOWN;
				}
			} else if (gravity == Gravity.HORIZONTAL_UP) {
				if (direction == Direction.LEFT) {
					gravity = Gravity.VERTICAL_LEFT;
				} else if (direction == Direction.RIGHT) {
					gravity = Gravity.VERTICAL_RIGHT;
				}

				if (absPosition > 0) {
					direction = Direction.DOWN;
				} else {
					direction = Direction.UP;
				}
			}

		} else if (height < width) {
			if (gravity == Gravity.VERTICAL_LEFT) {
				if (direction == Direction.UP) {
					gravity = Gravity.HORIZONTAL_UP;
				} else if (direction == Direction.DOWN) {
					gravity = Gravity.HORIZONTAL_DOWN;
				} else {
					switchGravity();
					return true;
				}

				if (absPosition > 0) {
					direction = Direction.RIGHT;
				} else {
					direction = Direction.LEFT;
				}
			} else if (gravity == Gravity.VERTICAL_RIGHT) {
				if (direction == Direction.UP) {
					gravity = Gravity.HORIZONTAL_UP;
				} else if (direction == Direction.DOWN) {
					gravity = Gravity.HORIZONTAL_DOWN;
				} else {
					switchGravity();
					return true;
				}

				if (absPosition > 0) {
					direction = Direction.LEFT;
				} else {
					direction = Direction.RIGHT;
				}
			}
		} else {
			return false;
//			switchGravity();
		}

		return true;
	}

	public boolean isShield() {
		return isShield;
	}

	public void setTouched(boolean isTouched) {
		this.isTouched = isTouched;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Entity : ").append("(").append(super.toString()).append(")")
				.append(" ; ").append("Speed : ").append(speed).append(" ; ")
				.append("Gravity : ").append(gravity).append(" ; ")
				.append("Direction : ").append(direction).append(" ; ")
				.append("Life : ").append(life).append(" ; ")
				.append("OnWall : ").append(onWall).append(" ; ")
				.append("IsJump : ").append(isJump).append(" ; ")
				.append("IsFall : ").append(isfall).append(" ; ");
		return sb.toString();
	}
}
