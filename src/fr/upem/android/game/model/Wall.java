package fr.upem.android.game.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import fr.upem.android.collision.BoundingBox;
import fr.upem.android.game.R;

/**
 * Wall model.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class Wall extends Entity {

	/**
	 * The constructor.
	 * 
	 * @param left
	 *            Left.
	 * @param right
	 *            Right.
	 * @param top
	 *            Top.
	 * @param bottom
	 *            Bottom.
	 * @param x
	 *            Coordinate x.
	 * @param y
	 *            Coordinate y.
	 * @param gravity
	 *            Gravity.
	 * @param entityType
	 *            EntityType.
	 */
	public Wall(int left, int right, int top, int bottom, int x, int y,
			Gravity gravity, EntityType entityType) {
		super(x, y, Bitmap.createBitmap(Math.abs(right - left),
				Math.abs(bottom - top), Bitmap.Config.ARGB_8888), entityType);
		this.x = x;
		this.y = y;
		this.bitmap = Bitmap.createBitmap(Math.abs(right - left),
				Math.abs(bottom - top), Bitmap.Config.ARGB_8888);
		bitmap.eraseColor(R.color.blueback);
		this.box = new BoundingBox(x, y, bitmap.getWidth(), bitmap.getHeight());
		this.entityType = entityType;
	}

	@Override
	public void onDrawEntity(Canvas canvas) {
		Paint p = new Paint();
		canvas.drawBitmap(bitmap, x, y, p);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Entity : ").append("(").append(super.toString()).append(")");
		return sb.toString();
	}
}