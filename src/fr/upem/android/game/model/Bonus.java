package fr.upem.android.game.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import fr.upem.android.collision.BoundingBox;

/**
 * Bonus model.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class Bonus extends Entity {

	/**
	 * If the bonus is blinked.
	 */
	protected boolean isBlink;

	/**
	 * If the bonus is active.
	 */
	protected boolean isActive;

	/**
	 * the timer.
	 */
	protected long timer;

	/**
	 * The timeElapsed.
	 */
	protected long timeElapsed;

	/**
	 * The constructor.
	 * 
	 * @param x
	 *            Coordinate x.
	 * @param y
	 *            Coordinate y.
	 * @param bitmap
	 *            The bitmap.
	 * @param entityType
	 *            The entity type.
	 * @param timer
	 *            The timer.
	 */
	public Bonus(int x, int y, Bitmap bitmap, EntityType entityType, long timer) {
		super(x, y, bitmap, entityType);
		this.x = x;
		this.y = y;
		this.bitmap = bitmap;
		this.entityType = entityType;
		this.box = new BoundingBox(x, y, bitmap.getWidth(), bitmap.getHeight());
		this.isBlink = false;
		this.isActive = true;
		this.timer = timer;
	}

	@Override
	public void onDrawEntity(Canvas canvas) {
		Paint p = new Paint();
		p.setAntiAlias(true);
		canvas.drawBitmap(bitmap, x, y, p);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Entity : ").append("(").append(super.toString()).append(")")
				.append(" ; ").append("TimeElapsed : ").append(timeElapsed)
				.append(" ; ").append("IsActive : ").append(isActive)
				.append(" ; ").append("IsBlank : ").append(isBlink)
				.append(" ; ");
		return sb.toString();
	}

	public boolean isActive() {
		return isActive;
	}
}