package fr.upem.android.game.model;

import android.graphics.Bitmap;
import fr.upem.android.collision.BoundingBox;
import fr.upem.android.game.graphics.Sprite;

/**
 * The entity model.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public abstract class Entity implements Sprite {

	/**
	 * Coordinate x.
	 */
	protected int x;

	/**
	 * Coordinate y.
	 */
	protected int y;

	/**
	 * Bitmap.
	 */
	protected Bitmap bitmap;

	/**
	 * Precise the entity type.
	 */
	protected EntityType entityType;

	/**
	 * The boundingbox (collisions).
	 */
	protected BoundingBox box;

	/**
	 * Constructor.
	 * 
	 * @param x
	 *            The coordinate x.
	 * @param y
	 *            The coordinate y.
	 * @param bitmap
	 *            The bitmap.
	 * @param entityType
	 *            The entityType.
	 * 
	 */
	public Entity(int x, int y, Bitmap bitmap, EntityType entityType) {
		this.x = x;
		this.y = y;
		this.entityType = entityType;
		this.bitmap = bitmap;
		if (entityType.equals(EntityType.WALL)) {
			bitmap.eraseColor(android.graphics.Color.GRAY);
		}
		this.box = new BoundingBox(x, y, bitmap.getWidth(), bitmap.getHeight());
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public BoundingBox getBox() {
		return box;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("X : ").append(x).append(" ; ").append("Y : ").append(y)
				.append(" ; ").append("EntityType : ").append(entityType)
				.append(" ; ").append("Box : ").append("(").append(box)
				.append(")");
		return sb.toString();
	}
}
