package fr.upem.android.game.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import fr.upem.android.game.MainActivity;
import fr.upem.android.game.R;
import fr.upem.android.util.I18n;

/**
 * HelpActivity - Shows Help.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class HelpActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Internationalization i18n.
		String langage = I18n.getLangage();
		I18n.updateLangage(getBaseContext(), langage);
		if (langage.equals("fr")) {
			setContentView(R.layout.menu_help_activity_fr);
		} else {
			setContentView(R.layout.menu_help_activity);
		}

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

	}

	@Override
	public void onBackPressed() {
		startActivity(new Intent(this, MainActivity.class)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK));
		return;
	}
}
