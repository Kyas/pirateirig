package fr.upem.android.game;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import fr.upem.android.collision.Collision;
import fr.upem.android.game.model.Bonus;
import fr.upem.android.game.model.GameMap;
import fr.upem.android.game.model.Pirate;
import fr.upem.android.game.model.Wall;
import fr.upem.android.util.Displays;
import fr.upem.android.util.DownloadMap;
import fr.upem.android.util.RespawnBonusTask;

/**
 * GameView. The custom view when we use a surfaceView for the game.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback {

	/**
	 * The context.
	 */
	private final Context context;

	/**
	 * The surfaceHolder.
	 */
	private final SurfaceHolder holder;

	/**
	 * The GameMapThread.
	 */
	private Thread gameMapThread;

	/**
	 * The gameMapRunnable.
	 */
	private Runnable gameMapRunnable;

	/**
	 * The GameMap.
	 */
	private GameMap map;

	/**
	 * The width.
	 */
	private final int width;

	/**
	 * The height.
	 */
	private final int height;

	/**
	 * The list of walls.
	 */
	private final ArrayList<Wall> walls;

	/**
	 * The array of Pirates.
	 */
	private final Pirate[] pirates;

	/**
	 * The ratio (horizontal).
	 */
	private final int ratioH;

	/**
	 * The ratio (vertical).
	 */
	private final int ratioV;

	/**
	 * The ratioEntity.
	 */
	private final int ratioEntity;

	/**
	 * The array Game.
	 */
	private final char[][] arrayGame;

	/**
	 * The bonus.
	 */
	private Bonus bonus;

	/**
	 * The running game (boolean).
	 */
	private boolean running = true;

	/**
	 * The jump.
	 */
	private final boolean[] jump = { false, false };

	/**
	 * If we are in Practice Mode.
	 */
	private final boolean isPracticeMode;

	/**
	 * The handler.
	 */
	private final Handler handler = new Handler();

	/**
	 * The runnable timer.
	 */
	private final Runnable runnableTimer;

	/**
	 * The timeout. (end of the game).
	 */
	private final static int TIMEOUT = 60000;

	private final static int RESPAWN_BONUS_TIME = 15000;

	/**
	 * The constructor.
	 * 
	 * @param context
	 *            The context.
	 * @param attrs
	 *            The attributeSet.
	 */
	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		isInEditMode();
		this.context = context;
		this.holder = getHolder();

		// Retrieve the screen size.
		int[] screens = Displays.getScreenSize(context);
		width = screens[0];
		height = screens[1];

		holder.addCallback(this);
		setFocusable(true);

		// Retrieve the bundle from the previous activity.
		// Here the selected file and the mode.
		Bundle receiveBundle = ((Activity) getContext()).getIntent()
				.getExtras();
		String contentFile = receiveBundle.getString("file");
		isPracticeMode = receiveBundle.getBoolean("isPracticeMode");

		// Retrieve the object DownloadMap from an AsyncTask.
		DownloadMap dMap = (DownloadMap) new DownloadMap(context, contentFile,
				width, height).execute();

		try {
			this.map = dMap.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		// Retrieve objects.
		this.pirates = map.getPirates();
		this.walls = map.getWalls();
		this.ratioH = map.getRatioH();
		this.ratioV = map.getRatioV();
		this.ratioEntity = (width / height);
		this.arrayGame = map.getArrayGame();

		// Runnable Timer.
		runnableTimer = new Runnable() {
			@Override
			public void run() {
				startScore();
			}
		};

		// Stop the game after one minute.
		handler.postDelayed(runnableTimer, TIMEOUT);

	}

	/**
	 * Allows to determine the id of the pirate relates TouchEvent
	 * 
	 * @param eventX
	 *            x in TouchEvent.
	 * @return 0 if TouchEvent is for pirate 1 <br>
	 *         1 if TouchEvent is for pirate 2 <br>
	 *         -1 Otherwise
	 * 
	 */
	private int idPirateForJump(int eventX) {
		if ((eventX >= 0) && (eventX <= width / 2)) {
			return 0;
		}

		if ((eventX >= width / 2) && (eventX <= width)) {
			return 1;
		}
		return -1;
	}

	/**
	 * OnTouchEvent (Delimiter Zone).
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		for (int i = 0; i < event.getPointerCount(); i++) {
			int eventX = (int) event.getX(i);
			// int eventY = (int) event.getY(i);

			int id = idPirateForJump(eventX);

			if (pirates[id].isFall()) {
				continue;
			}

			if (jump[id]) {
				boolean onWall = false;
				int epsilon = pirates[id].getBox().getHeight();

				for (int k = 0; k < walls.size(); k++) {
					// b = pirates[j].getBox().neartToWall(
					// walls.get(k).getBox().getRect(), epsilon);
					onWall = pirates[id].getBox().nearToWall(
							walls.get(k).getBox(), epsilon);
					if (onWall) {
						pirates[id].goFast(System.currentTimeMillis(), false);
						return true;
					}
				}
			}

			if (!jump[id]) {
				jump[id] = true;
				pirates[id].jump(true);
			}
		}
		return true;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

		holder = getHolder();
		holder.addCallback(this);

		gameMapRunnable = new gameMapRunnable(holder);
		gameMapThread = new Thread(gameMapRunnable);
		setRunning(true);
		gameMapThread.start();

		// For Version V2.
		// readyForGame();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		setRunning(false);

		while (retry) {
			try {
				// Pause the game.
				gameMapThread.join();
				retry = false;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Resume the game.
	 */
	public void onResume() {
		gameMapThread = new Thread(gameMapRunnable);
		setRunning(true);
		gameMapThread.start();
	}

	/**
	 * Pause the game.
	 */
	public void onPause() {
		boolean retry = true;
		setRunning(false);

		while (retry) {
			try {
				gameMapThread.join();
				retry = false;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sets the value represent if the game is in execution.
	 * 
	 * @param running
	 *            true if the game is running, false otherwise.
	 * 
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}

	/**
	 * The gameMapRunnable.
	 * 
	 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
	 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
	 * @version 1.0
	 * 
	 */
	class gameMapRunnable implements Runnable {

		private SurfaceHolder holder;

		/**
		 * The constructor.
		 * 
		 * @param holder
		 *            The surface holder.
		 */
		public gameMapRunnable(SurfaceHolder holder) {
			this.holder = holder;
		}

		@Override
		public void run() {
			Canvas canvas = null;

			// Effects of background map.
			Bitmap bmp = Bitmap.createBitmap(Math.abs(width / 2),
					Math.abs(height), Bitmap.Config.ARGB_4444);
			Bitmap bmp2 = Bitmap.createBitmap(Math.abs(width / 2),
					Math.abs(height), Bitmap.Config.ARGB_4444);
			Paint paint = new Paint();
			Matrix matrix = new Matrix();

			bmp.eraseColor(0xCCFFE4E1);
			bmp2.eraseColor(0xBFD1F6D8);
			paint.setARGB(240, 255, 255, 255);

			// The Looper for the AsyncTask.
			Looper.prepare();
			if (bonus == null) {
				// Use the respaxn bonus Task.
				callRespawnBonusTask();
			} else {
				if (!bonus.isActive()) {
					bonus = null;
				}
			}

			// The start timer.
			long startTimer = System.currentTimeMillis();

			while (running && pirates[0].getLife() > 0
					&& pirates[1].getLife() > 0) {

				// Timer of the game.
				long currentTimer = System.currentTimeMillis();
				int seconds = (int) ((currentTimer - startTimer) / 1000) % 60;
				int minutes = (seconds / 60) % 60;

				// Change the textView of the Timer.
				((GameActivity) getContext())
						.setTimerTextView(minutes, seconds);

				try {
					canvas = holder.lockCanvas();
					synchronized (holder) {

						if (canvas == null) {
							continue;
						}
						canvas.drawBitmap(bmp, matrix, paint);
						canvas.drawBitmap(bmp2, (float) width / 2, (float) 0,
								paint);

						/**
						 * Walls
						 */
						for (int i = 0; i < walls.size(); i++) {
							walls.get(i).onDrawEntity(canvas);
						}

						/**
						 * Multi-pirates
						 */

						for (int i = 0; i < pirates.length; i++) {

							if (jump[i]) {
								jump[i] = pirates[i].jump(true);
							} else if (pirates[i].onWall()) {
								pirates[i].move();
							}
							
							pirates[i].onDrawEntity(canvas);

							boolean onWall = false;
							for (int j = 0; j < walls.size(); j++) {

								if (!onWall) {
									// b = pirates[i].getBox().onTheWall(
									// walls.get(j).getBox().getRect());
									onWall = pirates[i].getBox().onTheWall(
											walls.get(j).getBox());
									pirates[i].setOnWall(onWall);
								}

								Collision.checkCollision(pirates[i],
										walls.get(j), isPracticeMode);
							}

							if (bonus != null) {
								if (Collision.checkCollision(pirates[i], bonus,
										isPracticeMode)) {
									bonus = null;
								}
							}
							if (!pirates[i].isJump()) {
								jump[i] = false;
							}

							if (onWall || !jump[i]) {
								pirates[i].jump(false);
							}

							if ((!onWall) && (!jump[i])) {
								pirates[i].fall();
							}

							Collision.checkCollision(pirates[0], pirates[1],
									isPracticeMode);

							// Change the textView of the Pirates' life.
							((GameActivity) getContext())
									.setPlayerLifeTextView(
											pirates[0].getLife(),
											pirates[1].getLife());
						}

						/**
						 * Bonus
						 */
						if (bonus != null) {
							bonus.onDrawEntity(canvas);
						}
					}

				} finally {
					// in case of an exception the surface is not left in
					// an inconsistent state
					if (canvas != null) {
						holder.unlockCanvasAndPost(canvas);
					}
				} // end finally
			}
			if (pirates[0].getLife() <= 0 || pirates[1].getLife() <= 0) {
				startScore();
			}
		}
	}

	/**
	 * Use a TimerTask which executes every 15 seconds the respawn of a Bonus.
	 * (random position / random bonus).
	 */
	private void callRespawnBonusTask() {

		// final Handler handler = new Handler(Looper.getMainLooper());
		Timer timer = new Timer();
		TimerTask doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {

				handler.post(new Runnable() {
					public void run() {
						try {
							RespawnBonusTask respawnBonusTask = new RespawnBonusTask();
							respawnBonusTask.execute(context, ratioH, ratioV,
									ratioEntity, arrayGame);
							bonus = respawnBonusTask.get();
						} catch (Exception e) {
							// Nothing
						}
					}
				});
			}
		};
		timer.schedule(doAsynchronousTask, 0, RESPAWN_BONUS_TIME); // execute in
																	// in every
		// 15000
	}

	public void stopTimerTask() {
		handler.removeCallbacks(runnableTimer);
	}

	/**
	 * Start the score activity with the current score.
	 */
	private void startScore() {
		Bundle receiveBundle = ((Activity) getContext()).getIntent()
				.getExtras();
		String contentFile = receiveBundle.getString("file");
		final int lifePlayerOne = pirates[0].getLife();
		final int lifePlayerTwo = pirates[1].getLife();

		if (lifePlayerOne <= 0 || lifePlayerOne < lifePlayerTwo) {
			// Victory of Player 2.
			setRunning(false);
			handler.removeCallbacks(runnableTimer);
			Intent intent = new Intent(context, ScoreActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("victory", 1);
			intent.putExtra("file", contentFile);
			context.startActivity(intent);
		} else if (lifePlayerTwo <= 0 || lifePlayerTwo < lifePlayerOne) {
			// Victory of Player 1.
			setRunning(false);
			handler.removeCallbacks(runnableTimer);
			Intent intent = new Intent(context, ScoreActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("victory", 0);
			intent.putExtra("file", contentFile);
			context.startActivity(intent);
		} else if (lifePlayerTwo == lifePlayerOne) {
			// Draw
			setRunning(false);
			handler.removeCallbacks(runnableTimer);
			Intent intent = new Intent(context, ScoreActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("victory", 2);
			intent.putExtra("file", contentFile);
			context.startActivity(intent);
		}
	}

	/**
	 * Amelioration for Version V2. (Pause the game for 3 seconds at the
	 * beginning).
	 */
	@SuppressWarnings("unused")
	private void readyForGame() {
		// final Handler handler = new Handler();
		final Timer timer = new Timer();
		final TimerTask task = new TimerTask() {
			public void run() {
				handler.post(new Runnable() {
					public void run() {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								context);
						builder.setIcon(R.drawable.ic_launcher);
						AlertDialog alert = builder.create();
						alert.show();
						setRunning(false);
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						onResume();
					}
				});
			}
		};
		timer.schedule(task, 0);
	}

}
