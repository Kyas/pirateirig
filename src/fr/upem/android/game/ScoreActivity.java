package fr.upem.android.game;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * ScoreActivity - Display the socre of the pirates.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class ScoreActivity extends Activity implements
		android.view.View.OnClickListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_score);

		// Full screen.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		final int victory = getIntent().getExtras().getInt("victory");
		final TextView victoryP1Text = (TextView) findViewById(R.id.score_victory_p1);
		final TextView victoryP2Text = (TextView) findViewById(R.id.score_victory_p2);
		final TextView victoryDrawText = (TextView) findViewById(R.id.score_victory_draw);
		final Button buttonReplay = (Button) findViewById(R.id.score_button_replay);
		final Button buttonBackMenu = (Button) findViewById(R.id.score_button_back_menu);

		switch (victory) {
		case 0:
			// Victory of P1
			victoryP1Text.setVisibility(View.VISIBLE);
			victoryP2Text.setVisibility(View.GONE);
			victoryDrawText.setVisibility(View.GONE);
			break;
		case 1:
			// Victory of P2
			victoryP1Text.setVisibility(View.GONE);
			victoryP2Text.setVisibility(View.VISIBLE);
			victoryDrawText.setVisibility(View.GONE);
			break;
		case 2:
			// Draw
			victoryP1Text.setVisibility(View.GONE);
			victoryP2Text.setVisibility(View.GONE);
			victoryDrawText.setVisibility(View.VISIBLE);
		}

		buttonReplay.setOnClickListener(this);
		buttonBackMenu.setOnClickListener(this);

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.score_button_replay:
			// Replay the game.

			AlertDialog.Builder builderRestart = new AlertDialog.Builder(this);
			builderRestart.setTitle(R.string.menu_restart);
			builderRestart.setNegativeButton(R.string.game_alert_cancel, null);
			builderRestart.setPositiveButton(R.string.menu_restart,
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Bundle receiveBundle = getIntent().getExtras();
							String contentFile = receiveBundle
									.getString("file");

							Intent intent = new Intent(ScoreActivity.this,
									GameActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK
									| Intent.FLAG_ACTIVITY_NO_ANIMATION);
							intent.putExtra("file", contentFile);
							finish();
							overridePendingTransition(android.R.anim.fade_in, 0);
							startActivity(intent);
						}
					});

			builderRestart.setMessage(R.string.game_alert_restart);
			builderRestart.setIcon(R.drawable.ic_launcher);
			builderRestart.create().show();
			break;
		case R.id.score_button_back_menu:
			// Back to the menu.

			AlertDialog.Builder builderMainMenu = new AlertDialog.Builder(this);
			builderMainMenu.setTitle(R.string.menu_main);
			builderMainMenu.setNegativeButton(R.string.game_alert_cancel, null);
			builderMainMenu.setPositiveButton(R.string.game_alert_ok,
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(ScoreActivity.this,
									MainActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK
									| Intent.FLAG_ACTIVITY_NO_ANIMATION);
							overridePendingTransition(android.R.anim.fade_in, 0);
							startActivity(intent);
						}
					});

			builderMainMenu.setMessage(R.string.game_alert_main_menu);
			builderMainMenu.setIcon(R.drawable.ic_launcher);
			builderMainMenu.create().show();
			break;
		}
	}
}
