package fr.upem.android.game;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import fr.upem.android.game.model.Level;
import fr.upem.android.game.model.LevelAdapter;
import fr.upem.android.util.LoadFile;

/**
 * SelectionLevelActivity. The list of level activities.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class SelectionLevelActivity extends Activity implements
		OnItemClickListener {
	/**
	 * The list of levels.
	 */
	private List<Level> levels;

	/**
	 * The mode.
	 */
	private boolean isPracticeMode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selection_level);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		isPracticeMode = getIntent().getExtras().getBoolean("isPracticeMode");

		final ListView listView = (ListView) findViewById(R.id.levels_list);
		listView.setOnItemClickListener(this);
		try {
			levels = LoadFile.listRaw();
		} catch (IllegalArgumentException e) {
			Log.e("IllegalArgumentException", e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			Log.e("IllegalAccessException", e.getLocalizedMessage());
		}
		
		LevelAdapter adapter;
		try {
			adapter = new LevelAdapter(this, levels);
			listView.setAdapter(adapter);
		} catch (IllegalArgumentException e) {
			Log.e("IllegalArgumentException", e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			Log.e("IllegalAccessException", e.getLocalizedMessage());
		}
	}

	@Override
	public void onBackPressed() {
		startActivity(new Intent(this, MainActivity.class)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK));
		return;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		InputStream is = getResources().openRawResource(
				levels.get(position).getResId());
		builder.setTitle(String.format(
				getResources().getString(
						R.string.selection_level_item_alert_title),
				levels.get(position).getFilename()));
		String contentFile = null;
		try {
			contentFile = LoadFile.readTextFile(is, false);

		} catch (IOException e) {
			e.printStackTrace();
		}
		builder.setNegativeButton(R.string.game_alert_cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		final String s = contentFile;
		builder.setPositiveButton(R.string.game_alert_ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						/**
						 * Appel de la generation de la map.
						 */
						if (s != null) {

							Intent intent = new Intent(
									SelectionLevelActivity.this,
									GameActivity.class);

							intent.putExtra("file", s);
							intent.putExtra("isPracticeMode", isPracticeMode);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK);

							startActivity(intent);
							// Toast.makeText(getApplicationContext(),
							// "Chargement du Niveau",
							// Toast.LENGTH_SHORT).show();
						}
					}
				});
		builder.setIcon(R.drawable.ic_launcher);
		AlertDialog alert = builder.create();
		alert.show();
	}
}
