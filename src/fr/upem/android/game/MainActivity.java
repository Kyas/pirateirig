package fr.upem.android.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import fr.upem.android.game.menu.CreditsActivity;
import fr.upem.android.game.menu.HelpActivity;
import fr.upem.android.util.I18n;

/**
 * MainActivity. List of Menus.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Internationalization i18n.
		String langage = I18n.getLangage();
		I18n.updateLangage(getBaseContext(), langage);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// Buttons
		final Button buttonNewGame = (Button) findViewById(R.id.button_new_game);
		final Button buttonPractice = (Button) findViewById(R.id.button_practice);
		final Button buttonHelp = (Button) findViewById(R.id.button_help);
		final Button buttonCredits = (Button) findViewById(R.id.button_credits);

		// ImageViews
		final ImageView imageViewFrench = (ImageView) findViewById(R.id.french_flag_icon);
		final ImageView imageViewEnglish = (ImageView) findViewById(R.id.uk_flag_icon);

		buttonNewGame.setText(R.string.button_new_game);
		buttonPractice.setText(R.string.button_practice);
		buttonHelp.setText(R.string.button_help);
		buttonCredits.setText(R.string.button_credits);

		imageViewFrench.setClickable(true);
		imageViewEnglish.setClickable(true);

		buttonNewGame.setOnClickListener(this);
		buttonPractice.setOnClickListener(this);
		buttonHelp.setOnClickListener(this);
		buttonCredits.setOnClickListener(this);
		imageViewFrench.setOnClickListener(this);
		imageViewEnglish.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Internationalization i18n.
		String langage = I18n.getLangage();
		I18n.updateLangage(getBaseContext(), langage);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		Intent intentSelection = new Intent(MainActivity.this,
				SelectionLevelActivity.class);
		Intent intent = getIntent();
		switch (v.getId()) {
		case R.id.button_new_game:
			// New Game - Mode.

			intentSelection.putExtra("isPracticeMode", false);
			startActivity(intentSelection);
			break;
		case R.id.button_practice:
			// Practice - Mode.

			intentSelection.putExtra("isPracticeMode", true);
			startActivity(intentSelection);
			break;
		case R.id.button_help:
			// Help - Mode.

			Intent intentHelp = new Intent(MainActivity.this,
					HelpActivity.class);
			startActivity(intentHelp);
			break;
		case R.id.button_credits:
			// Credits - Mode.

			Intent intentCredits = new Intent(MainActivity.this,
					CreditsActivity.class);
			startActivity(intentCredits);
			break;
		case R.id.french_flag_icon:
			// French langage.

			I18n.updateLangage(this, "fr");
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_NO_ANIMATION);
			finish();
			overridePendingTransition(android.R.anim.fade_in, 0);
			startActivity(intent);
			break;
		case R.id.uk_flag_icon:
			// English langage.

			I18n.updateLangage(this, "en");
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_NO_ANIMATION);
			finish();
			overridePendingTransition(android.R.anim.fade_in, 0);
			startActivity(intent);
			break;
		}

	}
}
