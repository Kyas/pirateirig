package fr.upem.android.collision;

import android.graphics.Rect;

/**
 * Defines the end of an object (pirates, wall, bonuses) allows to manage
 * collisions. The x, y is in the middle of the image.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class BoundingBox {
	/**
	 * The width of a box.
	 */
	private final int width;

	/**
	 * The height of a box.
	 */
	private final int height;

	/**
	 * Coordinate x.
	 */
	private int x;

	/**
	 * Coordinate y.
	 */
	private int y;

	/**
	 * Defines a rectangle around an object.
	 * 
	 * @param x
	 *            Coordinate X rectangle (upper left corner)
	 * @param y
	 *            Coordinate Y rectangle (upper left corner)
	 * @param width
	 *            Width of the rectangle (lower right corner)
	 * @param height
	 *            Height of the rectangle (lower right corner)
	 */
	public BoundingBox(int coordX, int coordY, int width, int height) {
		this.x = coordX;
		this.y = coordY;
		this.width = width;
		this.height = height;
	}

	/**
	 * Allows to know if our boundingBox intersects another boundingBox
	 * 
	 * @param box
	 *            Box with which must be realize the test
	 * 
	 * @return true if the two intersect boundingBox <br>
	 *         false otherwise *
	 */
	public boolean containsEntity(BoundingBox box) {
		Rect r = new Rect(x, y, x + width, y + height);
		return r.intersect(box.x, box.y, box.x + box.width, box.y + box.height);
	}

	/**
	 * Allows to know if our boundingBox is on a wall.
	 * 
	 * @param box
	 *            Box representative wall
	 * 
	 * @return true if our boundingBox is a 1 pixel wall
	 */
	public boolean onTheWall(BoundingBox box) {
		return nearToWall(box, 1);
	}

	/**
	 * Allows to know if our boundingBox is a "epsilon" of a wall.
	 * 
	 * @param box
	 *            Box representative wall
	 * @param epsilon
	 *            Maximum Distance to Wall
	 * 
	 * @return true if our boundingBox is less than "epsilon" a wall <br>
	 *         False otherwise *
	 */
	public boolean nearToWall(BoundingBox box, int epsilon) {
		Rect rect = new Rect(x, y, x + width, y + height);
		Rect r = new Rect(box.x, box.y, box.x + box.width, box.y + box.height);
		return r.intersects(rect.left - epsilon, rect.top - epsilon, rect.right
				+ epsilon, rect.bottom + epsilon);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Display the BoundinxBox.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("X : ").append(x).append(" ; ").append("Y : ").append(y)
				.append(" ; ").append("Width : ").append(width).append(" ; ")
				.append("Height : ").append(height);
		return sb.toString();
	}

}
