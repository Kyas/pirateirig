package fr.upem.android.collision;

import fr.upem.android.game.model.Direction;
import fr.upem.android.game.model.Entity;
import fr.upem.android.game.model.EntityType;
import fr.upem.android.game.model.Gravity;
import fr.upem.android.game.model.Pirate;

/**
 * Manage the different cases after collisions.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public class Collision {

	/**
	 * Constant speed of the pirates.
	 */
	private final static int CONSTANT_SPEED = 7;

	/**
	 * Allows you to check the collision between two entities.
	 * 
	 * @param e1
	 *            First entity.
	 * @param e2
	 *            Second entity.
	 * @param isPracticeMode
	 *            If we are in a GAME Mode or PRACTICE Mode. (desactivate life's
	 *            player).
	 * @return
	 */
	public static boolean checkCollision(Entity e1, Entity e2,
			boolean isPracticeMode) {

		BoundingBox box1 = e1.getBox();
		BoundingBox box2 = e2.getBox();

		boolean collision = box1.containsEntity(box2);

		if (collision) {

			EntityType entity1 = e1.getEntityType();
			EntityType entity2 = e2.getEntityType();

			// Collision between two players.
			if ((entity1 == EntityType.PLAYER_ONE)
					&& (entity2 == EntityType.PLAYER_TWO)) {
				Pirate pirate1 = (Pirate) e1;
				Pirate pirate2 = (Pirate) e2;
				pirate1.setTouched(true);
				pirate2.setTouched(true);

				// Direction change.
				if (pirate1.getDirection() == pirate2.getDirection()) {
					Direction direction = pirate1.getDirection();

					switch (direction) {
					case DOWN:
						if (pirate1.getY() < pirate2.getY()) {
							pirate1.switchDirection();
							pirate1.move();
						} else {
							pirate2.switchDirection();
							pirate2.move();
						}
						break;

					case UP:
						if (pirate1.getY() > pirate2.getY()) {
							pirate1.switchDirection();
							pirate1.move();
						} else {
							pirate2.switchDirection();
							pirate2.move();
						}
						break;

					case LEFT:
						if (pirate1.getX() > pirate2.getX()) {
							pirate1.switchDirection();
							pirate1.move();
						} else {
							pirate2.switchDirection();
							pirate2.move();
						}
						break;

					case RIGHT:
						if (pirate1.getX() < pirate2.getX()) {
							pirate1.switchDirection();
							pirate1.move();
						} else {
							pirate2.switchDirection();
							pirate2.move();
						}
						break;

					default:
						break;
					}
				} else {
					pirate1.switchDirection();
					pirate2.switchDirection();
					pirate1.move();
					pirate2.move();
				}

				// Decrementing the Pirate's Life
				if (!pirate1.isShield() && !pirate2.isShield()) {
					if (pirate1.getSpeed() > pirate2.getSpeed()) {
						if (isPracticeMode == false) {
							pirate2.decrementeLife();
						}
					} else if (pirate2.getSpeed() > pirate1.getSpeed()) {
						if (isPracticeMode == false) {
							pirate1.decrementeLife();
						}
					}
				}
			}

			// Collision between a player and ...
			else if ((entity1 == EntityType.PLAYER_ONE)
					|| (entity1 == EntityType.PLAYER_TWO)) {

				Pirate pirate = (Pirate) e1;
				// ... a wall
				if (entity2 == EntityType.WALL) {

					// Direction change
					if (!pirate.isFall() && !pirate.isJump()) {
						pirate.switchDirection();
						pirate.move();
						return true;
					}

					if (pirate.isJump()) {
						if(pirate.switchGravityAndDirection(box2)) {
							pirate.jump(false);
						}
						else {
							return collision;
						}
					}
					pirate.setFall(false);
					pirate.setSpeed(CONSTANT_SPEED);

					// Recalibration if collision into a wall
					Gravity gravity = pirate.getGravity();
					switch (gravity) {
					case HORIZONTAL_DOWN:
						if (box2.getY() < box1.getY() + box1.getHeight()) {
							if (box1.getX() >= box2.getX()) {
								int y1 = (box1.getY() + box1.getHeight() - box2
										.getY());
								pirate.moveAt(0, -y1);
							}
						}
						break;

					case HORIZONTAL_UP:
						if (box1.getY() < box2.getY() + box2.getHeight()) {
							if (box1.getX() >= box2.getX()) {
								int y1 = (box2.getY() + box2.getHeight() - box1
										.getY());
								pirate.moveAt(0, y1);
							}
						}
						break;

					case VERTICAL_LEFT:
						if (box1.getX() < box2.getX() + box2.getWidth()) {
							if (box1.getY() >= box2.getY()) {
								int x1 = (box2.getX() + box2.getWidth() - box1
										.getX());
								pirate.moveAt(x1, 0);
							}
						}
						break;

					case VERTICAL_RIGHT:
						if (box2.getX() < box1.getX() + box1.getWidth()) {
							if (box1.getY() >= box2.getY()) {
								int x1 = (box1.getX() + box1.getWidth() - box2
										.getX());
								pirate.moveAt(-x1, 0);
							}
						}
						break;

					default:
						break;
					}
				}

				else { // ... a bonus
					switch (entity2) {
					case BOMB:
						if (isPracticeMode == false) {
							pirate.decrementeLife();
						}
						pirate.setTouched(true);
						break;

					case LIFE:
						if (isPracticeMode == false) {
							pirate.incrementeLife();
						}
						break;

					case SHIELD:
						pirate.setShield(true, System.currentTimeMillis());
						break;

					case TURBO:
						pirate.goFast(System.currentTimeMillis(), true);
						break;

					default:
						pirate.setSpeed(0);
						break;
					}
				}
			}
		}
		return collision;
	}
}
