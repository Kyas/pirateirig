package fr.upem.android.collision;

/**
 * Movable Interface.
 * 
 * @author Jeremy Lor (jlor@etudiant.univ-mlv.fr)
 * @author Maxime Deshagette (mdeshagette@etudiant.univ-mlv.fr)
 * @version 1.0
 * 
 */
public interface Movable {

	void moveAt(int directionX, int directionY);

	void move();
}
